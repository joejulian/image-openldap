# OpenLDAP container image

[![pipeline status](https://gitlab.com/joejulian/image-openldap/badges/master/pipeline.svg)](https://gitlab.com/joejulian/image-openldap/commits/master)

This is a container image for OpenLdap. By default, this container expects the slapd.conf file in /config and the database structure in /slapd.d.

Usage:

```
docker run -v $PWD/slapd.conf:/config/slapd.conf $PWD/data:/slapd.d registry.gitlab.com/joejulian/image-openldap:latest
```