FROM registry.gitlab.com/joejulian/docker-arch:latest
LABEL app openssl
LABEL os archlinux
MAINTAINER Joe Julian <me@joejulian.name>

RUN pacman -Syu --noconfirm --needed \
        db \
        openldap \
        perl
RUN pacman -Scc --noconfirm
VOLUME ["/config","/slapd.d"]
EXPOSE 10389
ENTRYPOINT [ "/usr/bin/slapd", "-u","ldap", "-g","ldap"]
CMD ["-h","ldap://:10389", "-F","/slapd.d", "-f","/config/slapd.conf", "-d","0x8100"]
